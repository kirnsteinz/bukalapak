<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\vendor;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class vendorcontroller extends Controller
{
    public function index()
    {
        $vendorList = vendor::all();
        return response()->json($vendorList, 200);
    }
    function new(Request $request){
        DB::beginTransaction();
        try{
            $this->validate($request, [
                'name' => 'required',
                'location' => 'required',
                'uid' => 'required'
            ]);            
            $new = new vendor;
            $new->name = $request->input('name');
            $new->uid = $request->input('uid');
            $new->location = $request->input('location');
            $new->detail = $request->input('detail');
            $new->save();

            DB::commit();
            return response()->json(["message"=>"success"], 200);            
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=>""+$e->getMessage()], 500);
        }
    }

    function edit(Request $request){
        DB::beginTransaction();
        try{
            $this->validate($request, [
                'name' => 'required',
                'location' => 'required',
                'uid' => 'required'
            ]);            
            $new = vendor::where('id',$request->id)->get()->first();
            $new->name = $request->input('name');
            $new->uid = $request->input('uid');
            $new->location = $request->input('location');
            $new->detail = $request->input('detail');
            $new->save();

            DB::commit();
            return response()->json(["message"=>"success"], 200);            
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=>""+$e->getMessage()], 500);
        }
    }

    function delete(Request $request){
        DB::beginTransaction();
        try{                        
            $new = vendor::where('id',$request->id)->get()->first();
            $new->delete();
            DB::commit();
            return response()->json(["message"=>"success"], 200);            
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=>""+$e->getMessage()], 500);
        }
    }
}
