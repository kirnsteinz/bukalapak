<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\lapaker;
use App\User;

class getlapaker extends Controller
{
    // somewhere in your controller
    public function getUser(Request $request)
    {
        $token = $request->input('token');
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        // the token is valid and we have found the user via the sub claim+
        $me = JWTAuth::parseToken()->toUser();
        return response()->json($me, 200);
    }
    public function login(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json(compact('token'), 200);
    }
    function register(Request $request){
        DB::beginTransaction();
        try{
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email'
            ]);
            
            $user = new User;
            $user->name = $request->input('name');
            // $user->address = $request->input('address');
            $user->email = $request->input('email');
            $user->password = bcrypt($request->input('password'));   
            $user->save();

            $token = JWTAuth::fromUser($user);
            DB::commit();
            return response()->json(["message"=>"success"], 200);            
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=>""+$e->getMessage()], 500);
        }   
    }
    function me(Request $request){
        $user = $this->getUser($request);
        $lapaker = lapaker::where('userid',$user->id)->get()->first();
        return response()->json(compact('lapaker'));
    }
    function profile(Request $request){
        $user = $this->getUser($request);
        
        DB::beginTransaction();
        try{
            $lapaker = lapaker::where('userid',$user->id)->get()->first();
            if(!$lapaker){
                $lapaker = new lapaker;
            }
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=>""+$e->getMessage()], 500);
        }
    }
}
