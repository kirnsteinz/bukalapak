<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\product;

class item extends Controller
{   
    function index(){
        $productList = product::all();
        return response()->json($productList, 200);
    }
    function byBrand(Request $request){
        $brand = $request->input('id');
        $productList = product::where('brand',$brand)->get();
        return response()->json($productList, 200);
    }
    function byCategory(Request $request){
        $category = $request->input('id');
        $productList = product::where('category',$category)->get();
        return response()->json($productList, 200);
    }
    function byVendor(Request $request){
        $vendor = $request->input('id');
        $productList = product::where('vendor',$vendor)->get();
        return response()->json($productList, 200);
    }
    function byId(Request $request){
        $vendor = $request->input('id');
        $productList = product::where('vendor',$vendor)->get()->first();
        return response()->json($productList, 200);
    }
    function newproduct(Request $request){
        DB::beginTransaction();
        try{
            $this->validate($request, [
                'name' => 'required',
                'price' => 'required',
                'category' => 'required',
                'weight' => 'required'
            ]);            
            $new = new product;
            $new->name = $request->input('name');
            $new->price = intval($request->input('price'));
            $new->category = intval($request->input('category'));
            $new->vendor = intval($request->input('vendor'));
            $new->brand = $request->input('brand');
            $new->detail = $request->input('detail');
            $new->stock = intval($request->input('stock'));
            $new->weight = $request->input('weight');  
            $new->view = 0;
            $new->bought = 0; 
            $new->save();

            DB::commit();
            return response()->json(["message"=>"success"], 200);            
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=>""+$e->getMessage()], 500);
        }   
    }
    function edit(Request $request){
        DB::beginTransaction();
        try{
            $this->validate($request, [
                'name' => 'required',
                'price' => 'required',
                'category' => 'required',
                'weight' => 'required'
            ]);            
            $new = product::where('id',$request->input('id'))->get()->first();
            $new->name = $request->input('name');
            $new->price = $request->input('price');
            $new->category = $request->input('category');
            $new->vendor = $request->input('vendor');
            $new->brand = $request->input('brand');
            $new->detail = $request->input('detail');
            $new->stock = $request->input('stock');
            $new->weight = $request->input('weight');   
            $new->save();

            DB::commit();
            return response()->json(["message"=>"success", 'token' =>$token], 200);            
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=>""+$e->getMessage()], 500);
        } 
    }
    function delete(Request $request){
        DB::beginTransaction();
        try{
            $new = product::where('id',$request->input('id'))->get()->first();
            $new->delete();

            DB::commit();
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=>""+$e->getMessage()], 500);
        } 
    }
}
