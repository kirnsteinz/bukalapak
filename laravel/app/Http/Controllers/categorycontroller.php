<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\category;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class categorycontroller extends Controller
{
    public function index()
    {
        $categoryList = category::all();
        return response()->json($categoryList, 200);
    }
    function new(Request $request){
        DB::beginTransaction();
        try{
            $this->validate($request, [
                'name' => 'required'
            ]);            
            $new = new category;
            $new->name = $request->input('name');
            $parent = $request->input('parent');
            if (!$parent){
                $newparent = category::where('id',$parent)->get()->first();
                $new->level = $newparent->level +1;
            }
            else{
                $new->level = 0;
            }
            $new->save();

            DB::commit();
            return response()->json(["message"=>"success"], 200);            
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=>""+$e->getMessage()], 500);
        }
    }

    function edit(Request $request){
        DB::beginTransaction();
        try{
            $this->validate($request, [
                'name' => 'required'
            ]);            
            $new = category::where('id',$request->id)->get()->first();
            $new->name = $request->input('name');
            $parent = $request->input('parent');
            if (!$parent){
                $newparent = category::where('id',$parent)->get()->first();
                $new->level = $newparent->level +1;
            }
            else{
                $new->level = 0;
            }

            DB::commit();
            return response()->json(["message"=>"success"], 200);            
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=>""+$e->getMessage()], 500);
        }
    }

    function delete(Request $request){
        DB::beginTransaction();
        try{                        
            $new = category::where('id',$request->id)->get()->first();
            $new->delete();
            DB::commit();
            return response()->json(["message"=>"success"], 200);            
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=>""+$e->getMessage()], 500);
        }
    }
}
