<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\brand;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class brandcontroller extends Controller
{
    public function index()
    {
        $brandList = brand::all();
        return response()->json($brandList, 200);
    }
    function new(Request $request){
        DB::beginTransaction();
        try{
            $this->validate($request, [
                'name' => 'required',
                'detail' => 'required'
            ]);            
            $new = new brand;
            $new->name = $request->input('name');
            $new->detail = $request->input('detail');

            $image = $request->file('logo');
            $input['imagename'] = $new->name.'.'.time().'.'.$image->getClientOriginalExtension();
            $image->move('/brandlogo',$input['imagename']);

            $new->logo_pic = '/brandlogo/'.$input['imagename'];

            $new->save();

            DB::commit();
            return response()->json(["message"=>"success"], 200);            
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json([$e->getMessage()], 500);
        }
    }

    function edit(Request $request){
        DB::beginTransaction();
        try{
            $this->validate($request, [
                'name' => 'required',
                'detail' => 'required',
                'logo_pic' => 'required'
            ]);            
            $new = brand::where('id',$request->id)->get()->first();
            $new->name = $request->input('name');
            $new->detail = $request->input('detail');
            $new->logo_pic = $request->input('logo_pic');  
            $new->save();

            DB::commit();
            return response()->json(["message"=>"success"], 200);            
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json([$e->getMessage()], 500);
        }
    }

    function delete(Request $request){
        DB::beginTransaction();
        try{                        
            $new = brand::where('id',$request->id)->get()->first();
            $new->delete();
            DB::commit();
            return response()->json(["message"=>"success"], 200);            
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json([$e->getMessage()], 500);
        }
    }

}
