<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor')->unsigned();
            $table->integer('brand')->unsigned();
            $table->integer('category')->unsigned();
            $table->string('name');
            $table->bigInteger('price');
            $table->string('detail');
            $table->decimal('weight');
            $table->timestamps();
            $table->integer('bought');
            $table->integer('stock');
            $table->integer('view');

            $table->foreign('vendor')->references('id')->on('vendors');
            $table->foreign('brand')->references('id')->on('brands');
            $table->foreign('category')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
