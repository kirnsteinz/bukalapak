<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid')->unsigned();
            $table->integer('vendor')->unsigned();
            $table->integer('status');
            $table->string('deliveryname');
            $table->string('delivery');
            $table->string('payment');
            $table->timestamps();
            $table->string('note');
            $table->integer('order_code')->unique();

            $table->foreign('vendor')->references('id')->on('vendors');
            $table->foreign('uid')->references('id')->on('lapakers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}
