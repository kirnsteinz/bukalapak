<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//product controller
Route::group(['prefix' => 'product'], function(){
    Route::get('/','item@index');
    Route::post('vendor','item@byVendor');
    Route::post('category','item@byCategory');
    Route::post('brand','item@byBrand');

    Route::post('new','item@newproduct');
    //->middleware('jwt.auth')
    Route::post('edit','item@edit'); 
    Route::post('delete','item@delete'); 
});
//user & lapaker controller
Route::group(['prefix' => 'lapaker'], function(){
    // Route::get('me','getlapaker@me');
    // Route::get('user','getlapaker@getUser');
    // Route::get('profile','getlapaker@profile');

    Route::post('login','getlapaker@login');
    Route::post('register','getlapaker@register');    
    Route::post('edit','getlapaker@saveprofile');
});

//Me user controller
Route::post('me', ['before' => 'jwt-auth', function() {

    $user = JWTAuth::parseToken()->toUser();

    return Response::json(compact('user'));
}]);

//brand controller
Route::group(['prefix' => 'brand'], function(){
    Route::get('/','brandcontroller@index')->middleware('jwt.auth');

    Route::post('new','brandcontroller@new');
    Route::post('edit','brandcontroller@edit'); 
    Route::post('delete','brandcontroller@delete'); 
});

// //category controller
Route::group(['prefix' => 'category'], function(){
    Route::get('/','categorycontroller@index');

    Route::post('new','categorycontroller@new');//->middleware('jwt.auth')
    Route::post('edit','categorycontroller@edit'); 
    Route::post('delete','categorycontroller@delete'); 
});

//vendors controller
Route::group(['prefix' => 'vendor'], function(){
    Route::get('/','vendorcontroller@index');

    Route::post('new','vendorcontroller@new');//->middleware('jwt.auth')
    Route::post('edit','vendorcontroller@edit'); 
    Route::post('delete','vendorcontroller@delete'); 
});