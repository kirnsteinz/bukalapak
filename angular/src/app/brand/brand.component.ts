import { Component, OnInit } from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.css']
})
export class BrandComponent implements OnInit {

  constructor(private http:Http) { }

  files:FileList;
  nama : string;
  detail : string;

  ngOnInit() {
  }

  fileChange(event){
    this.files = event.target.files;
    //console.log(this.files);
  }

  upload(){
    console.log(this.files[0].name);

    let formdata  = new FormData();
    formdata.append("name", this.nama);
    formdata.append("detail", this.detail);
    formdata.append("logo", this.files[0],this.files[0].name);
    let header = new Headers();
    let option = new RequestOptions({headers:header});

    this.http.post('http://localhost:8000/api/brand/new', formdata, option)
    .subscribe(result=>console.log(result.json()));

  }
}
